<%@ page import="login.User" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
</head>
<body>   
	<div class="container">    
		<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info" >
				<div class="panel-heading">
					<div class="panel-title text-center">Sign In</div>
				</div>     
				<div style="padding-top:30px" class="panel-body" >
					<g:if test="${flash.message == 'Error'}">
							<div class="alert alert-danger" role="alert">Username and Password donot match</div> 
					</g:if>
					<g:elseif test="${flash.message}">
							<div class="alert alert-success" role="alert">${flash.message}</div> 
					</g:elseif>
					<form id="loginform" class="form-horizontal" action="./login" method="POST" role="form">
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username">                                        
						</div>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="login-password" type="password" class="form-control" name="password" placeholder="password">
						</div>
						<div style="margin-top:10px" class="form-group">
							<!-- Button -->
							<div class="col-sm-12 controls">
							  <p class="text-center"><button id="btn-login" type="submit" class="btn btn-success">Login</button></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 control text-center">
								<div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
									Don't have an account! 
								<a href="#" onClick="window.location='./signup'">
									Sign Up Here
								</a>
								</div>
							</div>
						</div>    
					</form>     
				</div>                     
			</div>  
		</div>
	</div>
</body> 
</html>
 