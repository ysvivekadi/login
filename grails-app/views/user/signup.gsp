<%@ page import="login.User" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
</head>
<body>   
	<div class="container">    
		<div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Sign Up</div>
					<div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="window.location='./index'">Sign In</a></div>
				</div>  
				<div class="panel-body" >
					<form id="signupform" action="./save" method="POST" class="form-horizontal" role="form">				
						<g:if test="${flash.message}">
							<div class="alert alert-danger" role="alert">${flash.message}</div> 
						</g:if>
						<div class="form-group">
							<label for="inputError" class="col-md-3 control-label">Email</label>
							<div class="col-md-9">
								<input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-md-3 control-label">Name</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="username" placeholder="Name">
							</div>
						</div> 
						<div class="form-group">
							<label for="password" class="col-md-3 control-label">Password</label>
							<div class="col-md-9">
								<input type="password" class="form-control" name="password" placeholder="Password">
							</div>
						</div> 
						<div class="form-group">
							<!-- Button -->                                        
							<div class="col-md-offset-3 col-md-9 text-center">
								<button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>Sign Up</button> 
							</div>
						</div> 
					</form>
			 	</div>
			</div>
		</div> 
	</div>
</body> 
</html>
 