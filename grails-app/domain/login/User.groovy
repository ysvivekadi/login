package login

class User {
	String username;
	String password;
	String email;
    static constraints = {
    	username(blank:false)
    	password(blank:false)
    	email(email:true, blank:false) 
    }
    String toString(){
    	return username
    }
}
